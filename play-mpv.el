;;; play-mpv.el --- mpv shortcuts

;;; Commentary:


;;; Code:

(require 'hydra)

(defun my-mpv (&optional l)
  "play a video with mpv"
  (interactive)
  (if (eq l nil)
      ;; TODO: add pull from clipboard
      (shell-command (format "mpv --ontop --no-border --autofit=500x200 --geometry=-15-60 %s &" (read-string "Enter url:")))
    (shell-command (concat "mpv --ontop --no-border --autofit=500x200 --geometry=-15-60 " l " &"))))

(defhydra play-mpv ()
  "play mpv"
  ("f" (my-mpv "https://www.youtube.com/freecodecamp/live") "freecodecamp Radio")
  ("g" (my-mpv) "get url"))

(provide 'play-mpv)
;;; play-mpv.el ends here
